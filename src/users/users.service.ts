import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.usersRepository.save(createUserDto);
  }

  findAll() {
    return this.usersRepository.find();
  }

  findOne(id: number) {
    return this.usersRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    //Find
    const user = await this.usersRepository.findOneBy({ id: id });

    //CheckError
    if (!user) {
      throw new NotFoundException();
    }

    //update
    const updadetedProduct = { ...user, ...updateUserDto };
    return this.usersRepository.save(updadetedProduct);
  }

  async remove(id: number) {
    //Find
    const user = await this.usersRepository.findOneBy({ id: id });

    //CheckError
    if (!user) {
      throw new NotFoundException();
    }

    //delete
    return this.usersRepository.softRemove(user);
  }
}
