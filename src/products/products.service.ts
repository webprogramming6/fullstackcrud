import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find();
  }

  findOne(id: number) {
    return this.productsRepository.findOne({ where: { id: id } });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    //Find
    const product = await this.productsRepository.findOneBy({ id: id });

    //CheckError
    if (!product) {
      throw new NotFoundException();
    }

    //update
    const updadetedProduct = { ...product, ...updateProductDto };
    return this.productsRepository.save(updadetedProduct);
  }

  async remove(id: number) {
    //Find
    const product = await this.productsRepository.findOneBy({ id: id });

    //CheckError
    if (!product) {
      throw new NotFoundException();
    }

    //delete
    return this.productsRepository.softRemove(product);
  }
}
